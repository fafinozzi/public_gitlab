# coding=utf-8

#############################################
#                                           #
# Author: Fabrizio Finozzi                  #
# Email: fabrizio.finozzi@gmail.com         #
#                                           #
# Version: 0.1                              #
# Date: 10.02.2021                          #
#                                           #
# Simulation of the Solar System            #
#                                           #
#############################################

#########################################################################
#                                                                       #
# IMPORTANT                                                             #
#                                                                       #
# This software is distributed without any warranty.                    #
#                                                                       #
# The author is not liable for any damage caused directly               #
# or indirectly by the use or misuse of this software.                  #
#                                                                       #
#########################################################################

#############################################################
#                                                           #
# CHANGELOG                                                 #
#                                                           #
# 0.1 - In progress                                         #
#                                                           #
#############################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from astroquery.jplhorizons import Horizons
import datetime as dt
import cashkarp

class celestial_body:
    
    def __init__(self, name, color, init_loc):
        self.name = name
        self.init_loc = []
        self.color = color
        self.orbit_x = []
        self.orbit_y = []

        for i,val in enumerate(init_loc):
            self.init_loc.insert(i,val)

    def integrate_orbit(self,duration):
        self.elapsed_time = 0.0
        self.duration = duration
        self.dt_try = 1.e-1
        locdt = []
        locdt0 = []
        dlocdt = []

        for i,val in enumerate(self.init_loc):
            if i < 6:
                locdt0.insert(i,val)

        while self.elapsed_time < self.duration:
            
            dlocdt = cashkarp.derivs_cart(locdt0)

            dt_did,dt_next,locdt = cashkarp.rkqs(locdt0,dlocdt,self.dt_try)
            self.orbit_x.append(locdt[0])
            self.orbit_y.append(locdt[1])
            self.dt_try = dt_next
            self.elapsed_time += dt_did
            
            locdt0.clear()

            for i,val in enumerate(locdt):
                locdt0.insert(i,val)
        
class solarsystem:

    def __init__(self, start_date, sim_duration):
        self.date = start_date
        self.sim_duration = sim_duration
        self.list_celestial_bodies = []

    def add_celestial_body(self,celestial_body):
        self.list_celestial_bodies.append(celestial_body)

    def run_simulation(self):
        for obj in self.list_celestial_bodies:
            print(' --> Evolving orbit of planet {} \n'.format(obj.name))
            obj.integrate_orbit(self.sim_duration)    
            print(' --> Evolution of planet {} is complete \n'.format(obj.name))


if __name__ == '__main__':

    # ---> Define initial parameters

    # Starting date of the simulation
    starting_date = dt.date.today()

    # Ending date of the simulation
    ending_date = dt.date(2040, 12 , 31)

    # Duration given by the difference of the ending and starting dates
    duration = ending_date - starting_date
    sim_duration = duration.days    

    planet_init_loc = []
    colors = ['gray', 'brown', 'blue', 'red','orange']
    names = ['Mercury','Venus','Earth','Mars','Jupiter'] 

    # ---> Build solar system

    print("        \n")
    print("##### Build solar system ##### \n")
    print("        \n")   

    solar = solarsystem(starting_date,sim_duration)

    for i_planet,val_planet in enumerate(names):
        id_planet = i_planet+1
        planet_obj = Horizons(id=id_planet, location="@sun", id_type='id').vectors()

        for i_coord,val_coord in enumerate(['x', 'y', 'z','vx', 'vy', 'vz']):
            planet_init_loc.insert(i_coord,float(planet_obj[val_coord]))

        planet = celestial_body(val_planet, colors[i_planet], planet_init_loc)
        solar.add_celestial_body(planet)
        planet_init_loc.clear()
        print(' --> Planet {} added to the solar system simulation \n'.format(planet.name))

    print("        \n")
    print("##### Evolve solar system ##### \n")
    print("        \n")   

    sun = celestial_body('Sun', 'yellow', [0.,0.,0.,0.,0.,0.,])
    solar.run_simulation()
    solar.add_celestial_body(sun)
    
    #plt.style.use('dark_background')
    fig = plt.figure(figsize=[6, 6])
    ax = fig.add_subplot(111)
    ax.set_aspect('equal')
    ax.set_title('Solar System')
    for i_planet,val_planet in enumerate(solar.list_celestial_bodies):
        if val_planet.name != 'Sun':
            ax.plot(val_planet.orbit_x,val_planet.orbit_y,c=val_planet.color,label=val_planet.name)
        else:
            ax.scatter(0.0,0.0,c=val_planet.color,s=100,label=val_planet.name)
    ax.legend(loc=1)
    plt.xlabel('X (AU)')
    plt.ylabel('Y (AU)')
    plt.savefig('Solar_System.png')

else:
    print("Check where you are!")