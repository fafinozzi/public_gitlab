#!/usr/bin/env python3

import numpy as np

global __mass_sun
global __univ_grav_const

__mass_sun = 1.98840987e+30 # (kg)
__univ_grav_const = 6.6743e-11 #(m3/kg s2)

# Convert m --> AU and s --> days
__AU_to_m = 149597870700.0
__day_to_sec = 24*3600
__univ_grav_const=__univ_grav_const*__day_to_sec**2/__AU_to_m**3

# Routine computes force vector components in spherical coordinates
# with theta=0 identifying the equatorial plane
# The equations of motion are integrated in Cartesian coordinates using
# a fifth-order Runge method that returns the estimate of the local truncation error.

# ---> Keplerian potential

def force(r,theta,phi): # computed for unit mass
    fr = -__univ_grav_const*__mass_sun/r/r
    fv = 0.0
    fp = 0.0
    return fr, fv, fp

# ---> Routine that computes the transformation of array from spherical to cartesian coordinates
# With theta=0 identifying the equatorial plane

def array_sph_to_cart(r,theta,phi,fr,fv,fp):
    fx=fr*np.cos(theta)*np.cos(phi)-fv*np.sin(theta)*np.cos(phi)-fp*np.sin(phi)
    fy=fr*np.cos(theta)*np.sin(phi)-fv*np.sin(theta)*np.sin(phi)+fp*np.cos(phi)
    fz=fr*np.sin(theta)+fv*np.cos(theta)
    return fx,fy,fz

def compute_rtp(x,y,z):
    r = np.sqrt(x*x+y*y+z*z)
    theta = np.arcsin(z/r)

    if(y > 0.0):
       phi = np.arccos(x/r/np.cos(theta))
    else:
       phi = 2.0*np.pi-np.arccos(x/r/np.cos(theta))

    return r,theta,phi

def derivs_cart(loc0):

    dlocdt = []

    x = loc0[0]
    y = loc0[1]
    z = loc0[2]
    vx = loc0[3]
    vy = loc0[4]
    vz = loc0[5]

    r,theta,phi = compute_rtp(x,y,z)

    fr, fv, fp = force(r,theta,phi)
    forcer = fr
    forcev = fv/r
    forcep = fp/r/np.cos(theta)

    if (r == 0.0 or theta == np.pi/2.0 or theta == 1.5*np.pi):
        forcep = 0.0
    
    if(r == 0.0):
       forcev = 0.0

    fx,fy,fz = array_sph_to_cart(r,theta,phi,forcer,forcev,forcep)

    dlocdt.insert(0,vx)
    dlocdt.insert(1,vy)
    dlocdt.insert(2,vz)
    dlocdt.insert(3,fx)
    dlocdt.insert(4,fy)
    dlocdt.insert(5,fz)

    return dlocdt

# ---> Cash Karp method (Cash  J.R.  &  Karp  A.H.,  1990), built as depicted in Press et al. , 1996

def rkck(y,dydx,h):

    if len(y) != len(dydx):
        raise BaseException("Vector sizes are not the same!")

#    A2=0.2
#    A3=0.3
#    A4=0.6
#    A5=1.0
#    A6=0.875
    B21=0.2
    B31=3.0/40.0
    B32=9.0/40.0
    B41=0.3
    B42=-0.9
    B43=1.2
    B51=-11.0/54.0
    B52=2.5
    B53=-70.0/27.0
    B54=35.0/27.0
    B61=1631.0/55296.0
    B62=175.0/512.0
    B63=575.0/13824.0
    B64=44275.0/110592.0
    B65=253.0/4096.0
    C1=37.0/378.0
    C3=250.0/621.0
    C4=125.0/594.0
    C6=512.0/1771.0
    DC1=C1-2825.0/27648.0
    DC3=C3-18575.0/48384.0
    DC4=C4-13525.0/55296.0
    DC5=-277.0/14336.0
    DC6=C6-0.25

    ytemp = np.empty(len(y))
    y_array = np.asarray(y)
    dydx_array = np.asarray(dydx)

    ytemp=y_array+B21*h*dydx_array
    ak2 = np.asarray(derivs_cart(ytemp))
    ytemp=y_array+h*(B31*dydx_array+B32*ak2)
    ak3 = np.asarray(derivs_cart(ytemp))
    ytemp=y_array+h*(B41*dydx_array+B42*ak2+B43*ak3)
    ak4 = np.asarray(derivs_cart(ytemp))
    ytemp=y_array+h*(B51*dydx_array+B52*ak2+B53*ak3+B54*ak4)
    ak5 = np.asarray(derivs_cart(ytemp))
    ytemp=y_array+h*(B61*dydx_array+B62*ak2+B63*ak3+B64*ak4+B65*ak5)
    ak6 = np.asarray(derivs_cart(ytemp))

    yout=y_array+h*(C1*dydx_array+C3*ak3+C4*ak4+C6*ak6)
    yerr=h*(DC1*dydx_array+DC3*ak3+DC4*ak4+DC5*ak5+DC6*ak6)

    return yout,yerr

def rkqs(y,dydx,htry):
    __par_safety=0.9
    __par_pgrow=-0.2
    __par_pschrink=-0.25
    __par_errcon=1.89e-4
    __yscal = 1.e-3
    __eps = 1.e-6
    htemp = 0.0
    errmax=2.0
    yerr=[]

    n1 = len(y)
    n2 = len(dydx)

    if n1 != n2:
        raise BaseException("Arrays have different lengths")
    
    h=htry
    while errmax > 1.0 and h != 0.0:
        yout,yerr = rkck(y,dydx,h)
        errmax=max(abs(np.asarray(yerr)/__yscal))/__eps
        htemp=__par_safety*h*(errmax**__par_pschrink)
        if h > 0:
            h = max(abs(htemp),0.1*abs(h))
        elif h < 0:
            h = -max(abs(htemp),0.1*abs(h))

    if (errmax > __par_errcon):
        hnext=__par_safety*h*(errmax**__par_pgrow)
    else:
        hnext=5.0*h
    hdid=h
    return hdid,hnext,yout

if __name__ == "__main__":
    print("I prefer to be used as a module, but I can do some tests for you")