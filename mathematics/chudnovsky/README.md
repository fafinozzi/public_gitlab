# Software:
This software provides my implementation of the Chudnovsky algorithm (Chudnovsky, David; Chudnovsky, Gregory (1988), Approximation and complex multiplication according to ramanujan, Ramanujan revisited: proceedings of the centenary conference), which provides a fast method for calculating the digits of pi

##### Requirements:
* the code requires the following Python packages: math

