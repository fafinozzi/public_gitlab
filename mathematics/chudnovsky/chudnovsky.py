import math

def item(k):
    num = (-1.0)**k*math.factorial(6.0*k)*(545140134.0*k+13591409.0)
    den = math.factorial(3.0*k)*math.factorial(k)**3*(640320)**(3*k+1.5)
    return num/den

def summation(n):
    result = 0.0
    for i in range(n):
        result += item(i)
    return result

my_pi = 1/(12*summation(10))
print((math.pi-my_pi)/math.pi*100.0)
print(math.pi,my_pi)
