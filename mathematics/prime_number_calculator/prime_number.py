def primes_finder(n):
    # number range to be changed
    number_range = set(range(2,n+1))
    
    #empty list to append discovered primes to
    primes_list = []
    
    #while loop
    while number_range:
        
        # pop the lowest prime number from number_range set
        prime = number_range.pop()
        
        # append it to the list of prime numbers
        primes_list.append(prime)
        
        # compute the set of all multiples of the prime number
        multiples = set(range(prime*2,n+1,prime))
        
        # remove the multiples of the prime number from the number_range set
        number_range.difference_update(multiples)

    print(primes_list)
    prime_count = len(primes_list)
    largest_prime = max(primes_list)
    
    #summary
    print(f"There are {prime_count} prime numbers between 2 and {n}, the largest being {largest_prime}")
    
    return

number = int(input("Could you please provide the upper bound of the range where you want to find the prime numbers?:"))

# run the main function
primes_finder(number)