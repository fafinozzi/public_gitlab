# IBM Quantum Challenge - November 2020:
This software provides my implementation of the exercises part of IBM Quantum Challenge of November 2020. The solutions from IBM can be found at https://github.com/qiskit-community/IBMQuantumChallenge2020.

##### Requirements:
* simulation requires following Python packages: NumPy, Qiskit (https://qiskit.org/)