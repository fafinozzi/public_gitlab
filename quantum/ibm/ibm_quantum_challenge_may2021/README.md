# IBM Quantum Challenge - May 2021:
This software provides my implementation of the exercises part of IBM Quantum Challenge of May 2021. The solutions from IBM can be found at https://github.com/qiskit-community/ibm-quantum-challenge-2021/tree/main/solutions%20by%20authors.

##### Requirements:
* the code requires the following Python packages: NumPy, Qiskit (https://qiskit.org/)