# IBM Quantum Challenge - November 2021:
This software provides my implementation of the exercises part of IBM Quantum Challenge of November 2021.

##### Requirements:
* simulation requires following Python packages: NumPy, Qiskit (https://qiskit.org/)