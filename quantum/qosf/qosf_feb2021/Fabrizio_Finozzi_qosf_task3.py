# coding=utf-8

#############################################
#                                           #
# Author: Fabrizio Finozzi                  #
# Email: fabrizio.finozzi@gmail.com         #
#                                           #
# Version: 0.1                              #
# Date: 10.02.2021                          #
#                                           #
# Created for QOSF Mentorship Program       #
#                                           #
#############################################

#########################################################################
#                                                                       #
# IMPORTANT                                                             #
#                                                                       #
# This software is distributed without any warranty.                    #
#                                                                       #
# The author is not liable for any damage caused directly               #
# or indirectly by the use or misuse of this software.                  #
#                                                                       #
#########################################################################

#############################################################
#                                                           #
# CHANGELOG                                                 #
#                                                           #
# 0.1 - In progress                                         #
#                                                           #
#############################################################

import numpy as np
import itertools
import random


class circuit:

    """
    The class creates a circuit for a quantum system with a number of qubits equal to __total_qubits.
    """

    def __init__(self):

        """
        Initialize the circuit object and its properties. Namely:
            __total_qubits (int): number of qubits of quantum system.
            __dim (int): dimension of the Hilbert space 2**n, where n = __total_qubits.
            __list_operators (list on numpy arrays): list of operators part of the quantum circuit.     
            __list_keys_basis_vectors (list of string): list of endians of the canonical basis vectors.
            __list_values_basis_vectors (list of numpy arrays): list of canonical basis vectors.
        """

        self.__total_qubits = 0
        self.__dim = 0
        self.__list_operators = []        
        self.__list_keys_basis_vectors = []
        self.__list_values_basis_vectors = []

    def prepare_ground_state(self,num_qubits):

        """
        Prepare the initial state.

        Args:
            num_qubits (int): number of qubits of quantum system.

        Returns:
            ground_state (numpy array of dimension 2**num_qubits) with all zeroes except first element which is 1.

        Raises:
            ValueError exception if num_qubits <=0.
        """

        if num_qubits <= 0:
            raise ValueError('Cannot initialize state for less than one qubit.')

        self.__total_qubits = num_qubits
        self.__dim = 2**num_qubits
        ground_state = np.zeros((self.__dim), dtype=float)
        ground_state[0]=1.0
        return ground_state

    def add_onequbit_operator(self,operator_name,target_qubit,vartheta=None, varphi=None, varlambda=None):

        """
        Adds to the circuit object a single-qubit operator. The method makes use of the little endian ordering.

        Args:
            operator_name (str): name of the operator.
            target_qubit (int): index of the target qubit.
            vartheta (float), varphi (float), lambda (float): Euler angles.

        Returns:
            Adds to the circuit object a single-qubit operator.

        Raises:
            ValueError: if the operator name is not a string.
            ValueError: if the operator name is not supported.
            RunTimeError: if the size of the operator matrix is different from the dimension of the Hilbert space.
        """

        if type(operator_name) != str:
            raise ValueError('Please provide a valid operator name (string).')
        else:
            if operator_name == "pauli-x":
                op = np.array([[0.0, 1.0],[1.0, 0.0]])
            elif operator_name == "pauli-y":
                op = np.array([[0.0, -1j],[1j, 0.0]])
            elif operator_name == "pauli-z":
                op = np.array([[1.0, 0.0],[0.0, -1.0]])
            elif operator_name == "hadamard":
                op = 1.0/np.sqrt(2.0)*np.array([[1.0, 1.0],[1.0, -1.0]])
            elif operator_name == "rphi":
                op = np.array([[1.0, 0.0],[0.0, np.exp(-1j*varphi)]])
            elif operator_name == "u":
                op = np.array([[np.cos(vartheta/2), -np.exp(1j*varlambda)*np.sin(vartheta/2)],[np.exp(1j*varphi)*np.sin(vartheta/2), np.exp(1j*(varphi+varlambda))*np.cos(vartheta/2)]])
            else:
                raise ValueError("Operator not supported. Supported operators are pauli-x,pauli-y,pauli-z ,hadamard, rphi and u.")

        # Define the Identity Matrix of size two
        I = np.identity(2)

        # Define the operator acting on the target qubit
        ops_list = [I]*self.__total_qubits
        target_qubit_index = self.__total_qubits-target_qubit-1 # little endian ordering
        ops_list[target_qubit_index] = op
        operator = ops_list[0]

        for i,opval in enumerate(ops_list):
            if i>0:
                operator = np.kron(operator,opval)
            else:
                pass

        # Check whether the size of the operator is self.__dim, otherwise raise exception
        if operator.shape != (self.__dim,self.__dim):
            raise RuntimeError("The operator matrix size is different from the dimension of the Hilbert space.")
        self.__list_operators.append(operator)

    def add_cnot_operator(self,control_qubit,target_qubit):

        """
        Adds to the circuit object a Controlled-NOT operator. The method makes use of the little endian ordering.

        Args:
            control_qubit(int): index of the control qubit.
            target_qubit (int): index of the target qubit.

        Returns:
            Adds to the circuit object a Controlled-NOT operator.

        Raises:
            ValueError: if the indeces of the target and control qubits are the same.
            RunTimeError: if the size of the operator matrix is different from the dimension of the Hilbert space.
        """

        # Check whether target and control qubits are different
        if target_qubit == control_qubit:
            raise ValueError('Cannot use the same index for the target and control qubit.')
        
        # Define the Pauli-Z gate matrix representation for a single-qubit operation
        zero_state = np.array([1.0,0.0])
        one_state = np.array([0.0,1.0])
        zero_outer = np.outer(zero_state,zero_state)
        one_outer = np.outer(one_state,one_state)
        x_gate = np.array([[0.0, 1.0],[1.0, 0.0]])

        # Define the Identity Matrix with size self.__total_qubits
        I = np.identity(2)

        # Define the operator acting on the target qubit, taking into consideration the control_qubit
        target_qubit_index = self.__total_qubits-target_qubit-1
        control_qubit_index = self.__total_qubits-control_qubit-1
        ops_list_term_one = [I]*self.__total_qubits
        ops_list_term_two = [I]*self.__total_qubits
        ops_list_term_one[control_qubit_index] = zero_outer
        ops_list_term_two[control_qubit_index] = one_outer
        ops_list_term_two[target_qubit_index] = x_gate
        
        operator_term_one = ops_list_term_one[0]
        for i,opval in enumerate(ops_list_term_one):
            if i>0:
                operator_term_one = np.kron(operator_term_one,opval)
            else:
                pass

        operator_term_two = ops_list_term_two[0]
        for i,opval in enumerate(ops_list_term_two):
            if i>0:
                operator_term_two = np.kron(operator_term_two,opval)
            else:
                pass

        operator = operator_term_one+operator_term_two

        # Check whether the size of the operator is self.__dim, otherwise raise exception
        if operator.shape != (self.__dim,self.__dim):
            raise RuntimeError("The operator matrix size is different from the dimension of the Hilbert space.")
        
        self.__list_operators.append(operator)

    def run_program(self,initial_state):

        """
        Applies the operators added to the circuit object to the initial_state.

        Args:
            initial_state (numpy array of dimension 2**num_qubits) with all zeroes except first element which is 1.

        Returns:            
            final_state (numpy array of dimension 2**num_qubits) obtained by applying the operators added to the circuit object to the initial_state.

        Raises:
        """

        final_state = initial_state
        for op in self.__list_operators:
            final_state = np.dot(op,final_state)
        return final_state

    def initialize_canonical_basis(self):
        
        # Build the canonical basis vectors for an Hilbert space of dimension self.__total_qubits
        vec_zero = np.array([1,0])
        vec_one = np.array([0,1])
        
        # --> Create endians corresponding to the canonical basis vectors. For example, for self.__total_qubits=2, it retursn "00","01","10","11"
        self.__list_keys_basis_vectors = list(itertools.product(["0", "1"], repeat=self.__total_qubits))

        for i,keyval in enumerate(self.__list_keys_basis_vectors):
            string = ''.join(keyval)
            self.__list_keys_basis_vectors[i]=string
            
        # --> Create the canonical basis vectors
        vec_list=[]
        for lst in self.__list_keys_basis_vectors:
            
            vec_list=[vec_zero]*self.__total_qubits
            for i,el in enumerate(lst):
                if el == "1":
                    vec_list[i]=vec_one
            
            vec_basis=vec_list[0]
            for i,val in enumerate(vec_list):
                if i>0:
                    vec_basis=np.kron(vec_basis,val)
            self.__list_values_basis_vectors.append(vec_basis)

    def run_measurement(self,final_state):

        """
        Collapses the final_state (which is in a superposition) to one basis (canonical) state.

        Args:
            final_state (numpy array of dimension 2**num_qubits): obtained by applying the operators added to the circuit object to the initial_state.

        Returns:            
            key_basis_vector (string): endian of the basis (canonical state) onto which the final_state collapsed.

        Raises:
        """

        # Normalize the final_state (normalized to 100)
        normalized_state=(final_state/np.sum(final_state)*100).astype(int)
        
        # Create a sequence of the endians of the canonical basis vectors, based on the probabilities from the normalized final_state
        list_measurement = []
        temp_list=[]
        for i,key_val in enumerate(self.__list_keys_basis_vectors):
            temp_list.append(key_val)
            list_measurement += temp_list*normalized_state[i]
            del temp_list[0]

        # Draw a value from a weighted random distribution
        key_basis_vector = random.choice(list_measurement)

        return key_basis_vector

    def get_counts(self,state_vector,num_shots,circuit_object):

        """
        Executes the run_measurement method in a loop of num_shots.

        Args:
            state_vector (numpy array of dimension 2**num_qubits): obtained by applying the operators added to the circuit object to the initial_state.
            num_shots (int): number of times for which the run_measurement method is executed.

        Returns:            
            Statistics of the endians of the canonical basis vectors and the corresponding number of occurrences.

        Raises:
        """

        # Initialize the self.__list_keys_basis_vectors
        circuit_object.initialize_canonical_basis()

        dic_stats={}
        for i in self.__list_keys_basis_vectors:
            dic_stats[i]=0

        for _ in range(num_shots):
            string=circuit_object.run_measurement(state_vector)
            dic_stats[string]+=1
        
        for i in self.__list_keys_basis_vectors:
            print(i,dic_stats[i])

# ================ MAIN PROGRAM ==================

# Define the circuit object
my_circuit = circuit()

# Set the number of qubits
qubit_number = 4

# Initialize the state of the quantum system
initial_state = my_circuit.prepare_ground_state(qubit_number)

# Create a Bell state
my_circuit.add_onequbit_operator("hadamard",0)
my_circuit.add_cnot_operator(0,1)

# Apply the quantum gates to initial state
final_state = my_circuit.run_program(initial_state)

my_circuit.get_counts(final_state,1000,my_circuit)
