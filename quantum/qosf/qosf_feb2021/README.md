# Quantum Open Source Foundation - Quantum Mentorship Program - Screening Tasks February 2021:
This software provides an implementation of a quantum circuit simulator.

##### Features:
The simulator:
* initializes a state
* implements all basic gates one qubit gates and Controlled-NOT. For each gate:
	* calculates the matrix operator
	* applies the operator (i.e. modify the initial state)
* performs, using a weighted random technique, a multi-shot measurement of all qubits

Please note that the simulator makes use of the little endian ordering (https://en.wikipedia.org/wiki/Endianness).

##### Requirements:
* simulation requires following Python packages: NumPy, itertools, random


##### Future improvements: 
* add multi-qubit gates (more than two qubits)